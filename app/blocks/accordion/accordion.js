$(document).ready(function () {
  $(function () {
    $(".accordion__item h3").on("click", function (e) {
      e.preventDefault();
      var $this = $(this);

      if (!$this.hasClass("active")) {
        $(".accordion__content").slideUp(400);
        $(".accordion__item h3").removeClass("active");
      }
      $this.toggleClass("active");
      $this.next().slideToggle();
    });
  });
});
