//=require ../blocks/**/*.js
$(document).ready(function () {
  const $body = $("body");

  const anchors = document.querySelectorAll("a.scroll-link");
  for (let anchor of anchors) {
    anchor.addEventListener("click", function (e) {
      e.preventDefault();
      const blockID = anchor.getAttribute("href").substr(1);
      $(".js-nav").removeClass("active");
      $(".js-burger").removeClass("active");
      $("body").removeClass("overflow");
      document.getElementById(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
    });
  }

  //accept
  $(".js-open-accept").click(function () {
    $(".js-accept-modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  });

  //form
  $(".js-open-form").click(function () {
    $(".js-form-modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  });

  //popup
  $(".js-open-descr").on("click", function (e) {
    $(this).find(".modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  });

  //close modal
  $(".js-close-modal").on("click", function (e) {
    $(this).parents().find(".modal").removeClass("active");
    $(".js-mask").removeClass("active");
    $body.removeClass("overflow");
    return false;
  });
  
  $(".js-close-modal").click(function () {
    $(".modal").removeClass("active");
    $(".js-mask").removeClass("active");
    $body.removeClass("overflow");
  });

  //video
  var videoOne = $("#video-one").get(0);
  var videoTwo = $("#video-two").get(0);
  var videoThree = $("#video-three").get(0);
  var videoFour = $("#video-four").get(0);
  var videoFive = $("#video-five").get(0);

  $(".js-video-open").click(function () {
    $(this).parent().find(".js-video-modal").addClass("active");
    $(this).parent().find(".js-video").trigger("play");
    $(".js-video-mask").addClass("active");
    $body.addClass("overflow");
  });
  $(".js-video-close").click(function () {
    $(".js-video-modal").removeClass("active");
    $(".js-video-mask").removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
    videoThree.pause();
    videoThree.currentTime = 0;
    videoFour.pause();
    videoFour.currentTime = 0;
    videoFive.pause();
    videoFive.currentTime = 0;
  });

  $(".js-mask").click(function () {
    $(".modal").removeClass("active");
    $(this).removeClass("active");
    $body.removeClass("overflow");
  });

  //close video onclick mask
  $(".js-video-mask").click(function () {
    $(".js-video-modal").removeClass("active");
    $(this).removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
    videoThree.pause();
    videoThree.currentTime = 0;
    videoFour.pause();
    videoFour.currentTime = 0;
    videoFive.pause();
    videoFive.currentTime = 0;
  });

  //valid form
  function feedback() {
    $("#feedback_name").removeClass("field_err");
    $("#feedback_email").removeClass("field_err");
    $("#feedback_message").removeClass("field_err");
    $("#feedback_captcha").removeClass("field_err");

    if (!$("#feedback_name").val()) {
      $("#feedback_name").addClass("field_err");
      return false;
    }
    if (!$("#feedback_email").val()) {
      $("#feedback_email").addClass("field_err");
      return false;
    }
    let email = $("#feedback_email").val();

    if (email.length > 0 && (email.match(/.+?\@.+/g) || []).length !== 1) {
      $("#feedback_email").addClass("field_err");
      return false;
    }
    if (!$("#feedback_message").val()) {
      $("#feedback_message").addClass("field_err");
      return false;
    }
    if (!$("#feedback_captcha").val()) {
      $("#feedback_captcha").addClass("field_err");
      return false;
    }

    var fdata = new FormData();
    fdata.append("feedback_name", $("#feedback_name").val());
    fdata.append("feedback_email", $("#feedback_email").val());
    fdata.append("feedback_message", $("#feedback_message").val());
    fdata.append("feedback_captcha", $("#feedback_captcha").val());
    fdata.append(
      "_csrf-frontend",
      $('meta[name="csrf-token"]').attr("content")
    );

    $.ajax({
      url: "/feedback",
      type: "POST",
      data: fdata,
      //mimeType:"multipart/form-data",
      contentType: false,
      cache: false,
      dataType: "json",
      processData: false,
      success: function (data) {
        $("#popup_query").fadeOut();
        $("#popup_query_success").fadeIn().delay(3000).fadeOut();
        $(".js-mask").delay(3000).fadeOut();
      },
      error: function (data) {
        //alert(data.message);
        if (data.responseJSON.field == "feedback_name") {
          $("#feedback_name").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_email") {
          $("#feedback_email").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_message") {
          $("#feedback_message").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_captcha") {
          $("#feedback_captcha").addClass("field_err");
        }

        captcha_refresh();
        $("#feedback_captcha").val("");
      },
    });

    return false;
  }

  $(".js-valid").click(function () {
    feedback();
  });
});
