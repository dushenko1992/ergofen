"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

$(document).ready(function () {
  $(function () {
    $(".accordion__item h3").on("click", function (e) {
      e.preventDefault();
      var $this = $(this);

      if (!$this.hasClass("active")) {
        $(".accordion__content").slideUp(400);
        $(".accordion__item h3").removeClass("active");
      }

      $this.toggleClass("active");
      $this.next().slideToggle();
    });
  });
});
$(document).ready(function () {
  //burger
  if (document.querySelector(".js-burger")) {
    $(".js-burger").click(function () {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(".js-nav").removeClass("active");
        $("body").removeClass("overflow");
      } else {
        $(this).addClass("active");
        $(".js-nav").addClass("active");
        $("body").addClass("overflow");
      }
    });
  }

  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $(".header").addClass("active");
    } else {
      $(".header").removeClass("active");
    }
  });
});
$(document).ready(function () {
  //tabs
  $(function () {
    $("ul.js-tab-btn").on("click", "li:not(.active)", function () {
      $(this).addClass("active").siblings().removeClass("active").closest("div.js-tabs").find("div.js-tab-content").removeClass("active").eq($(this).index()).addClass("active");
    });
  });
});
$(document).ready(function () {
  var $body = $("body");
  var anchors = document.querySelectorAll("a.scroll-link");

  var _iterator = _createForOfIteratorHelper(anchors),
      _step;

  try {
    var _loop = function _loop() {
      var anchor = _step.value;
      anchor.addEventListener("click", function (e) {
        e.preventDefault();
        var blockID = anchor.getAttribute("href").substr(1);
        $(".js-nav").removeClass("active");
        $(".js-burger").removeClass("active");
        $("body").removeClass("overflow");
        document.getElementById(blockID).scrollIntoView({
          behavior: "smooth",
          block: "start"
        });
      });
    };

    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      _loop();
    } //accept

  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  $(".js-open-accept").click(function () {
    $(".js-accept-modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  }); //form

  $(".js-open-form").click(function () {
    $(".js-form-modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  }); //popup

  $(".js-open-descr").on("click", function (e) {
    $(this).find(".modal").addClass("active");
    $(".js-mask").addClass("active");
    $body.addClass("overflow");
  }); //close modal

  $(".js-close-modal").on("click", function (e) {
    $(this).parents().find(".modal").removeClass("active");
    $(".js-mask").removeClass("active");
    $body.removeClass("overflow");
    return false;
  });
  $(".js-close-modal").click(function () {
    $(".modal").removeClass("active");
    $(".js-mask").removeClass("active");
    $body.removeClass("overflow");
  }); //video

  var videoOne = $("#video-one").get(0);
  var videoTwo = $("#video-two").get(0);
  var videoThree = $("#video-three").get(0);
  var videoFour = $("#video-four").get(0);
  var videoFive = $("#video-five").get(0);
  $(".js-video-open").click(function () {
    $(this).parent().find(".js-video-modal").addClass("active");
    $(this).parent().find(".js-video").trigger("play");
    $(".js-video-mask").addClass("active");
    $body.addClass("overflow");
  });
  $(".js-video-close").click(function () {
    $(".js-video-modal").removeClass("active");
    $(".js-video-mask").removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
    videoThree.pause();
    videoThree.currentTime = 0;
    videoFour.pause();
    videoFour.currentTime = 0;
    videoFive.pause();
    videoFive.currentTime = 0;
  });
  $(".js-mask").click(function () {
    $(".modal").removeClass("active");
    $(this).removeClass("active");
    $body.removeClass("overflow");
  }); //close video onclick mask

  $(".js-video-mask").click(function () {
    $(".js-video-modal").removeClass("active");
    $(this).removeClass("active");
    $body.removeClass("overflow");
    videoOne.pause();
    videoOne.currentTime = 0;
    videoTwo.pause();
    videoTwo.currentTime = 0;
    videoThree.pause();
    videoThree.currentTime = 0;
    videoFour.pause();
    videoFour.currentTime = 0;
    videoFive.pause();
    videoFive.currentTime = 0;
  }); //valid form

  function feedback() {
    $("#feedback_name").removeClass("field_err");
    $("#feedback_email").removeClass("field_err");
    $("#feedback_message").removeClass("field_err");
    $("#feedback_captcha").removeClass("field_err");

    if (!$("#feedback_name").val()) {
      $("#feedback_name").addClass("field_err");
      return false;
    }

    if (!$("#feedback_email").val()) {
      $("#feedback_email").addClass("field_err");
      return false;
    }

    var email = $("#feedback_email").val();

    if (email.length > 0 && (email.match(/.+?\@.+/g) || []).length !== 1) {
      $("#feedback_email").addClass("field_err");
      return false;
    }

    if (!$("#feedback_message").val()) {
      $("#feedback_message").addClass("field_err");
      return false;
    }

    if (!$("#feedback_captcha").val()) {
      $("#feedback_captcha").addClass("field_err");
      return false;
    }

    var fdata = new FormData();
    fdata.append("feedback_name", $("#feedback_name").val());
    fdata.append("feedback_email", $("#feedback_email").val());
    fdata.append("feedback_message", $("#feedback_message").val());
    fdata.append("feedback_captcha", $("#feedback_captcha").val());
    fdata.append("_csrf-frontend", $('meta[name="csrf-token"]').attr("content"));
    $.ajax({
      url: "/feedback",
      type: "POST",
      data: fdata,
      //mimeType:"multipart/form-data",
      contentType: false,
      cache: false,
      dataType: "json",
      processData: false,
      success: function success(data) {
        $("#popup_query").fadeOut();
        $("#popup_query_success").fadeIn().delay(3000).fadeOut();
        $(".js-mask").delay(3000).fadeOut();
      },
      error: function error(data) {
        //alert(data.message);
        if (data.responseJSON.field == "feedback_name") {
          $("#feedback_name").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_email") {
          $("#feedback_email").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_message") {
          $("#feedback_message").addClass("field_err");
        } else if (data.responseJSON.field == "feedback_captcha") {
          $("#feedback_captcha").addClass("field_err");
        }

        captcha_refresh();
        $("#feedback_captcha").val("");
      }
    });
    return false;
  }

  $(".js-valid").click(function () {
    feedback();
  });
});